package eu.dindoffer.unsafe.example.gui;

import eu.dindoffer.unsafe.example.core.NativeStore;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.layout.VBox;

/**
 * FXML Controller class
 *
 * @author Martin Dindoffer <contact@dindoffer.eu>
 */
public class MainWindowController {

    private final NativeStore nativeStore = new NativeStore();

    @FXML
    private VBox myRoot;
    @FXML
    private ListView<Long> lviewAddresses;
    @FXML
    private Button btnStore;
    @FXML
    private Button btnRetrieve;

    @FXML
    private void onbtnStore(ActionEvent event) {
        long address = nativeStore.storeDataOffHeap();
        lviewAddresses.getItems().add(address);
    }

    @FXML
    private void onbtnRetrieve(ActionEvent event) {
        Long address = lviewAddresses.getSelectionModel().getSelectedItem();
        if (address != null) {
            nativeStore.retrieveDataOffHeap(address);
        }
        lviewAddresses.getItems().remove(address);
    }
}
