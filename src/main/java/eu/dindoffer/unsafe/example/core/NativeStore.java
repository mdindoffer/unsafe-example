package eu.dindoffer.unsafe.example.core;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Random;
import sun.misc.Unsafe;

/**
 *
 * @author Martin Dindoffer <contact@dindoffer.eu>
 */
public class NativeStore {

    private static final int ONE_MB = 1024 * 1024;

    private final Unsafe unsafeAccess;
    private final Random generator;

    public NativeStore() {
        Unsafe unsafe = null;
        try {
            Field f = Unsafe.class.getDeclaredField("theUnsafe");
            f.setAccessible(true);
            unsafe = (Unsafe) f.get(null);
        } catch (IllegalAccessException | IllegalArgumentException | NoSuchFieldException | SecurityException e) {
            e.printStackTrace();
        }
        this.unsafeAccess = unsafe;
        generator = new Random();
    }

    public long storeDataOffHeap() {
        byte[] data = generateData();
        long offHeapPointer = unsafeAccess.allocateMemory(ONE_MB);
        System.out.println("Native address: " + offHeapPointer);
        unsafeAccess.copyMemory(data, 0, null, offHeapPointer, ONE_MB);
        return offHeapPointer;
    }

    public void retrieveDataOffHeap(long address) {
        byte[] output = new byte[ONE_MB];
        unsafeAccess.copyMemory(null, address, output, 0, ONE_MB);
        unsafeAccess.freeMemory(address);
        System.out.println("Retrieved: " + Arrays.toString(output).substring(0, 120));
    }

    private byte[] generateData() {
        byte[] output = new byte[ONE_MB];
        generator.nextBytes(output);
        System.out.println("Generated: " + Arrays.toString(output).substring(0, 120));
        return output;
    }

}
