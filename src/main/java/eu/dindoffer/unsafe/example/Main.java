package eu.dindoffer.unsafe.example;

import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * -Xms64m
 * -Xmx64m
 * -XX:-UseGCOverheadLimit
 *
 * @author Martin Dindoffer
 */
public class Main extends Application {

    @Override
    public void start(Stage stage) throws IOException {
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("fxml/MainWindow.fxml"));
        Scene scene = new Scene(root);
        stage.setTitle("Unsafe example");
        stage.setScene(scene);
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
